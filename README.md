# JavaScript Checkout #

A simple store checkout to purchase apples and oranges, and calculate any offers that might be on

### Setup ###

* Clone the repo
* cd javascript-checkout
* npm install

Then simply open the html files in your browser and start purchasing. Tests are performed using Mocha and Chai by opening testrunner.html, but I ran out of time so was unable to build any adequate tests.