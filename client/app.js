'use strict';

// Polyfill for Object.assign to enable es5 support
if (typeof Object.assign != 'function') {
	Object.assign = function(target) {
		'use strict';
		if (target == null) {
			throw new TypeError('Cannot convert undefined or null to object');
		}

		target = Object(target);
		for (var index = 1; index < arguments.length; index++) {
			var source = arguments[index];
			if (source != null) {
				for (var key in source) {
					if (Object.prototype.hasOwnProperty.call(source, key)) {
						target[key] = source[key];
					}
				}
			}
		}
		return target;
	};
}



// Watch for add product clicks & populate cart
[].forEach.call(document.querySelectorAll('.add_product'), function(e) {
	e.addEventListener('click', function(event) {

		// Add product with corresponding id to the cart
		var itemToAdd =  
			_.filter(shop.products, function(o) { 
				return o.id == event.target.id.substr(11); 
			 });

	
		cart.push(Object.assign({}, itemToAdd[0] ))
	});
});

document.getElementById('checkout').addEventListener('click', function() {
	shop.calculateCart(cart, 'value');
});





/* 
	Return a summed value
	arrObjs is an array of objects
	target is the target value to be included in the sum
*/
var helpers = {
	
	sumObjValues: function sumObjValues(arrObjs, target) {
		return arrObjs
			.map(function(item) {
				return item[target];
			})
			.reduce(function(tally, next) {
				return tally += next
			}, 0);
	},

	countProduct: function countProduct(product, target, cart) {
		return _.filter(cart, function(o) {
				return o[target] == product;
		}).length;
	},

	twoForOne: function twoForOne(productCount) {
		return Math.floor(productCount / 2);
	},

	threeForTwo: function threeForTwo(productCount) {
		return Math.floor(productCount / 3);
	}

} 






// Array of objects to store products
var cart = [];


var shop = (function() {

	var publicAPI = {
		products: [
			{
				id: 1,
				name: 'Apple',
				value: 0.60
			}
			, {
				id: 2,
				name: 'Orange',
				value: 0.25
			}  
		],

		offers: {
			available: true,

		},
			// buy one, get one free on Apples
			// 3 for the price of 2 on Oranges

		
		
		calculateCart: function calculateCart(products, target) {
			
			var currency = `£${numeral(helpers.sumObjValues(products, target)).format('0,0.00')}`;
			shop.displaySubtotal(currency);
			
		},

		displaySubtotal: function displaySubtotal(val) {
			var target = document.getElementById('balance');
			target.innerHTML = `Your subtotal is ${val}`;

			if (shop.offers.available) {
				shop.offersCalculate();
			}

		},

		offersCalculate: function offersCalculate() {
			// Unsophistocated checking for number of apples to discount
			var freeApples = helpers.twoForOne(helpers.countProduct('Apple', 'name', cart));
			var freeOranges = helpers.threeForTwo(helpers.countProduct('Orange', 'name', cart));

			document.getElementById('offerApples').innerHTML = `Have ${freeApples} free Apples on us`;
			document.getElementById('offerOranges').innerHTML = `Have ${freeOranges} free Oranges on us`;
	
			
		}

	}
	
	return publicAPI;
})();